'use strict';
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const bodyParser = require('body-parser');

const app = express();

const dataAPI = require('./routes/data');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('port', 16902);

app.use('/data', dataAPI);

app.get('/test', (req, res) => {
    res.send('hackathon');
});

app.listen(app.get('port'), () => {
    console.log('server started at port at ' + app.get('port'));
});