'use strict';
const express = require('express');
const router  = express.Router();

router.route('/sound')
    .post((req, res) => {
    res.send('sound API called');
});

router.route('/attach')
    .post((req, res) => {
        res.send('attach API called');
});


module.exports = router;